Texte & musique&nbsp;: Étienne Michelet<br>
Monde&nbsp;: AAA<br>

Cette œuvre est également disponible en [d'autres formats](https://abrupt.cc/etienne-michelet/mudras-vampires/) et ses sources sont [librement accessibles](https://gitlab.com/abrupt/mudras-vampires/).
