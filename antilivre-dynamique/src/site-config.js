export const SITE = {
  author: 'Étienne Michelet & AAA',
  title: 'Mudras Vampires | Antilivre | Abrüpt',
  displayTitle: 'Mudras Vampires',
  description: 'Je suis devenu vampire par la pratique du seon. Mais je n’aurais jamais découvert le seon sans mener une existence vampire en Corée. Ce livre n’est pas un livre de développement personnel, ou il l’est, mais à l’usage des vampires.',
  url: 'https://www.antilivre.org/mudras-vampires/',
  titlelink: 'https://abrupt.cc/etienne-michelet/mudras-vampires/',
  lang: 'fr',
  locale: 'fr_FR',
  warning: 'warning',
  credits: 'Texte et musique&nbsp;: Étienne Michelet<br>Monde&nbsp;: AAA'
};

// // Theme configuration
// export const PAGE_SIZE = 10;
export const THREE_SITE = true;
