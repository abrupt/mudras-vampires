import SimpleBar from 'simplebar';
import * as ADJUST from '../tools/adjust';
import * as TOOLS from '../tools/tools.js';
import Sound from './Sound';

export default class Header {
  constructor(menuBtn, menuEl) {
    ADJUST.adjustVh();

    this.loadBtn = document.querySelector('.button--load');
    this.menuBtnOn = document.querySelector('.button--menu .button__svg--on');
    this.menuBtnOff = document.querySelector('.button--menu .button__svg--off');
    this.menuBtn = menuBtn;
    this.menu = menuEl;
    this.menuOpen = false;

    this.blocker = document.querySelector('.blocker');
    this.instructions = document.querySelector('.blocker__instructions');

    this.setMenu(this.menuBtn, this.menu);
    this.sound = new Sound();
  }

  loader() {
    this.loadBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.loadBtn.blur();
      document.querySelector('.loading').classList.add('loading--done');
    });
  }

  setMenu(btn, el) {
    // SimpleBar for the menu
    this.simpleBarEl = new SimpleBar(el);
    // Disable scroll simplebar for print
    window.addEventListener('beforeprint', () => {
      this.simpleBarEl.unMount();
    });
    window.addEventListener('afterprint', () => {
      this.simpleBarEl = new SimpleBar(el);
    });

    // Toggle the menu
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      btn.blur();
      this.menuBtnOn.classList.toggle('none');
      this.menuBtnOff.classList.toggle('none');
      this.menuOpen = !this.menuOpen;
      if (this.menuOpen) {
        this.instructions.style.display = 'none';
        this.blocker.style.display = 'none';
      } else if (!TOOLS.isTouchDevice()) {
        this.blocker.style.display = 'block';
        this.instructions.style.display = '';
      }
      el.classList.toggle('show--menu');
    });
  }
}
