import SimpleBar from 'simplebar';
import * as TOOLS from '../tools/tools.js';

export default class Text {
  constructor() {
    this.textOpen = false;
    this.texts = [...document.querySelectorAll('.text')];
    this.textContainer = document.querySelector('.texts');
    this.closeBtn = document.querySelector('.texts__close');

    // SimpleBar for the menu
    this.simpleBarEl = new SimpleBar(this.textContainer);
    // Disable scroll simplebar for print
    window.addEventListener('beforeprint', () => {
      this.simpleBarEl.unMount();
    });
    window.addEventListener('afterprint', () => {
      this.simpleBarEl = new SimpleBar(this.textContainer);
    });

    this.textContainer.addEventListener('mousemove', () => {
      this.mouseInsideText = true;
    });

    this.textContainer.addEventListener('mouseenter', () => {
      this.mouseInsideText = true;
    });

    this.textContainer.addEventListener('mouseleave', () => {
      this.mouseInsideText = false;
    });

    this.closeBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.closeText();
      this.closeBtn.blur();
    });
  }

  showText() {
    this.texts.forEach((el) => {
      el.classList.remove('show');
    });
    this.textContainer.classList.add('show');
    const randomText = TOOLS.randomNb(0, this.texts.length - 1);
    this.texts[randomText].classList.add('show');
    this.texts[randomText].scrollIntoView();
    this.textOpen = true;
  }

  closeText() {
    this.texts.forEach((el) => {
      el.classList.remove('show');
    });
    this.textContainer.classList.remove('show');
    this.textOpen = false;
  }
}
