import World from '../World.js';

export default class Time extends EventTarget {
  constructor() {
    super();

    // Setup
    this.start = Date.now();
    this.current = this.start;
    this.elapsed = 0;
    this.delta = 16;

    window.requestAnimationFrame(() => {
      this.animation();
    });
  }

  animation() {
    const currentTime = Date.now();
    this.delta = currentTime - this.current;
    this.current = currentTime;
    this.elapsed = this.current - this.start;

    if (this.delta > 60) {
      this.delta = 60;
    }

    this.dispatchEvent(new Event('animation'));

    window.requestAnimationFrame(() => {
      this.animation();
    });
  }
}
