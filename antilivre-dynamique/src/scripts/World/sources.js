import * as TOOLS from '../tools/tools.js';

const bg = TOOLS.randomNb(1, 10);

export default [
  {
    name: 'sculpture',
    type: 'gltfModel',
    path: 'three/models/sculpture/sculpture.gltf'
  },
];
