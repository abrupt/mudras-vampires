import * as THREE from 'three';
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';
import World from '../World.js';
import * as TOOLS from '../../tools/tools.js';

export default class Solids extends EventTarget {
  constructor() {
    super();

    this.world = new World();
    this.scene = this.world.scene;
    this.resources = this.world.resources;
    this.time = this.world.time;
    this.debug = this.world.debug;

    this.randomPosMinY = 600;
    this.randomPosMaxY = 800;
    this.randomPosMinYStart = 50;
    this.randomPosMaxYStart = 400;

    // Resource
    this.solidsList = [];

    this.params = {
      name: 'sculpture',
      shape: this.resources.items.sculpture.scene,
      scale: 200
    };

    this.setSculpture(this.params.shape, this.params.scale);
    this.setBackgroundObjects();

    // Debug
    // if (this.debug.active) {
    //   this.debugFolder = this.debug.ui.addFolder('solids');
    // }
    // if (this.debug.active) {
    //   this.debugFolder
    //     .add(this.params, 'name', { sculpture1: { shape: this.resources.items.sculpture1.scene, scale: 3.8 }, sculpture2: { shape: this.resources.items.sculpture2.scene, scale: 0.04 } })
    //     .name('Shape')
    //     .onChange((value) => {
    //       this.removeFlyingObjects();
    //       this.setFlyingObjects(value.shape, value.scale);
    //     });
    // }
  }

  setSolid(solid, position, scale) {
    solid.scale.set(scale.x, scale.y, scale.z);
    solid.position.set(position.x, position.y, position.z);
    // solid.position.set(position);
    this.scene.add(solid);
  }

  setSculpture(obj, scaleNb) {
    const solid = SkeletonUtils.clone(obj);

    const position = {
      // x: TOOLS.randomNb(this.randomPosMin, this.randomPosMax),
      // y: TOOLS.randomNb(this.randomPosStartMax, this.randomPosStartMaxAlt),
      // z: TOOLS.randomNb(this.randomPosMin, this.randomPosMax)
      x: 0,
      y: 170,
      z: -0,
    };

    const randomScale = scaleNb;
    const scale = {
      x: randomScale,
      y: randomScale,
      z: randomScale
    };

    // this.setSolid(solid, position, scale);

    solid.scale.set(scale.x, scale.y, scale.z);
    solid.position.set(position.x, position.y, position.z);

    solid.children[0].traverse((child) => {
      if (child instanceof THREE.Mesh && child.material instanceof THREE.MeshStandardMaterial) {
        child.material.transparent = true;
        child.material.opacity = 1;
        child.material.color = new THREE.Color(0xffffff);
        child.material.emissive = new THREE.Color(0xffffff);
        child.material.roughness = 0;
        child.material.metalness = 1;

        // child.material.roughness = 0;
        // child.material.transmission = 1;
        // child.material.thickness = 0.5;
      }
    });

    this.scene.add(solid);

    solid.speedRotation = 0.2;
    this.solidsList.push(solid);
  }

  buildTwistMaterial(amount) {
    const material = new THREE.MeshNormalMaterial();
    material.onBeforeCompile = function(shader) {
      shader.uniforms.time = { value: 0 };

      shader.vertexShader = `uniform float time;\n${shader.vertexShader}`;
      shader.vertexShader = shader.vertexShader.replace(
        '#include <begin_vertex>',
        [
          `float theta = sin( time + position.y ) / ${amount.toFixed(1)};`,
          'float c = cos( theta );',
          'float s = sin( theta );',
          'mat3 m = mat3( c, 0, s, 0, 1, 0, -s, 0, c );',
          'vec3 transformed = vec3( position ) * m;',
          'vNormal = vNormal * m;'
        ].join('\n')
      );

      material.userData.shader = shader;
    };

    // Make sure WebGLRenderer doesnt reuse a single program

    material.customProgramCacheKey = function() {
      return amount.toFixed(1);
    };

    return material;
  }

  setBackgroundObjects() {
  }

  update() {
    for (const solid of this.solidsList) {
      const time = this.time.elapsed / 1000;
      solid.rotation.y = time * solid.speedRotation;
    }
  }
}
