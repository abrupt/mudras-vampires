import * as THREE from 'three';
import { Water } from 'three/examples/jsm/objects/Water.js';
import { Sky } from 'three/examples/jsm/objects/Sky.js';
import World from '../World.js';

export default class Environment {
  constructor() {
    this.world = new World();
    this.scene = this.world.scene;
    this.renderer = this.world.renderer;
    this.resources = this.world.resources;
    this.debug = this.world.debug;

    this.sun = new THREE.Vector3();
    this.sky = new Sky();
    this.water = new Water();

    this.renderTarget;

    // Debug
    if (this.debug.active) {
      this.debugFolder = this.debug.ui.addFolder('environment');
    }

    this.params = {
      background: 0x111111,
      fog: 0x111111,
      water: 0x000000
    };

    this.setWater();
    this.setRain();
    this.setLight();
  }

  setLight() {
    this.ambientLight = new THREE.AmbientLight('#ffffff', 3); // soft white light
    this.scene.add(this.ambientLight);
    this.light = new THREE.HemisphereLight('#ffffff', '#000000', 3);
    this.light.position.set(0, 1000, 0);
    this.scene.add(this.light);

    this.sunLight = new THREE.DirectionalLight('#ffffff', 6);
    this.sunLight.position.set(1000, 1000, 1000);
    this.scene.add(this.sunLight);

    this.sunLight2 = new THREE.DirectionalLight('#ffffff', 6);
    this.sunLight2.position.set(-1000, 1000, -1000);
    this.scene.add(this.sunLight2);

    // const canvas = document.createElement('canvas');
    // canvas.width = 1;
    // canvas.height = 32;

    // const context = canvas.getContext('2d');
    // const gradient = context.createLinearGradient(0, 0, 0, 32);
    // gradient.addColorStop(0.0, '#bb0000');
    // gradient.addColorStop(0.5, '#dd0000');
    // gradient.addColorStop(1.0, '#ff0000');
    // context.fillStyle = gradient;
    // context.fillRect(0, 0, 1, 32);
    // const skyMap = new THREE.CanvasTexture(canvas);
    // skyMap.colorSpace = THREE.SRGBColorSpace;
    //
    // const sky = new THREE.Mesh(
    //   new THREE.SphereGeometry(4000),
    //   new THREE.MeshBasicMaterial({ map: skyMap, side: THREE.BackSide })
    // );
    // this.scene.add(sky);

    // Background
    this.scene.background = new THREE.Color(this.params.background);
    this.scene.fog = new THREE.FogExp2(this.params.fog, 0.002);
    // this.scene.fog = new THREE.Fog( this.params.fog, 0, 1200 );
  }

  setRain() {
    this.rainCount = 100000;
    this.vertices = [];
    for (let i = 0; i < this.rainCount; i++) {
      const x = THREE.MathUtils.randFloatSpread(2000);
      const y = THREE.MathUtils.randFloatSpread(2000);
      const z = THREE.MathUtils.randFloatSpread(2000);
      this.vertices.push(x, y, z);
    }
    this.rainGeometry = new THREE.BufferGeometry();
    this.rainGeometry.setAttribute('position', new THREE.Float32BufferAttribute(this.vertices, 3));


    this.sprite = new THREE.TextureLoader().load('three/textures/sprite.png');
    this.sprite.colorSpace = THREE.SRGBColorSpace;

    this.rainMaterial = new THREE.PointsMaterial({
      color: 0x777777,
      size: 0.5,
      map: this.sprite,
      sizeAttenuation: true,
      alphaTest: 0.5,
      transparent: true
    });

    this.rain = new THREE.Points(this.rainGeometry, this.rainMaterial);
    this.scene.add(this.rain);
  }

  setWater() {
    this.waterGeometry = new THREE.PlaneGeometry(10000, 10000);

    this.water = new Water(
      this.waterGeometry,
      {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: new THREE.TextureLoader().load('three/textures/waternormals.jpg', (texture) => {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        }),
        sunDirection: new THREE.Vector3(),
        sunColor: this.params.background,
        waterColor: this.params.water,
        distortionScale: 7,
        // fog: this.scene.fog
        fog: this.scene.fog !== undefined
      }
    );

    this.water.rotation.x = -Math.PI / 2;
    this.water.position.y = -10;

    this.scene.add(this.water);
  }

  update() {
    const time = Date.now() * 0.00007;

    this.water.material.uniforms.time.value += 1.0 / 60.0;

    for (let i = 0; i < this.scene.children.length; i++) {
      const object = this.scene.children[i];

      if (object instanceof THREE.Points) {
        object.rotation.x = time * (i < 4 ? i + 1 : -(i + 1));
      }
    }
  }
}
