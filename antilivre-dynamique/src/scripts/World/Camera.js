import * as THREE from 'three';
// import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { MapControls } from 'three/examples/jsm/controls/MapControls.js';
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls.js';
import World from './World.js';
import Text from '../Layout/Text.js';
import * as TOOLS from '../tools/tools.js';

export default class Camera {
  constructor() {
    this.world = new World();
    this.text = new Text();
    this.sizes = this.world.sizes;
    this.scene = this.world.scene;
    this.canvas = this.world.canvas;

    this.controls = null;

    this.movingCamera = false;
    this.movingCameraEnd = false;
    this.timer = null;

    this.menuOpen = true;

    this.moveForward = false;
    this.moveBackward = false;
    this.moveLeft = false;
    this.moveRight = false;
    this.prevTime = performance.now();
    this.velocity = new THREE.Vector3();
    this.direction = new THREE.Vector3();
    this.vertex = new THREE.Vector3();
    this.color = new THREE.Color();

    this.setInstance();
    this.setControls();

    document.addEventListener('keydown', (event) => { this.onKeyDown(event); });
    document.addEventListener('keyup', (event) => { this.onKeyUp(event); });

    this.controls.addEventListener('start', () => {
      this.movingCameraEnd = false;
    });

    this.controls.addEventListener('end', () => {
      clearTimeout(this.timer);
      this.movingCameraEnd = true;
      if (this.movingCamera) {
        this.movingCamera = false;
      }
    });

    this.controls.addEventListener('change', () => {
      if (this.movingCameraEnd) {
        clearTimeout(this.timer);
        this.movingCamera = false;
      } else {
        this.timer = setTimeout(() => {
          this.movingCamera = true;
        }, 150);
      }
    });
  }

  onKeyDown(event) {
    switch (event.code) {
      case 'ArrowUp':
      case 'KeyW':
        this.moveForward = true;
        break;

      case 'ArrowLeft':
      case 'KeyA':
        this.moveLeft = true;
        break;

      case 'ArrowDown':
      case 'KeyS':
        this.moveBackward = true;
        break;

      case 'ArrowRight':
      case 'KeyD':
        this.moveRight = true;
        break;

      case 'Enter':
        if (this.controls.isLocked === true) {
          this.text.showText();
        }
        break;

      case 'Backspace':
      case 'KeyX':
        if (this.controls.isLocked === true) {
          this.text.closeText();
        }
        break;

      case 'Space':
      case 'Period':
        if (this.menuOpen) {
          this.controls.unlock();
          this.menuOpen = !this.menuOpen;
        } else {
          this.controls.lock();
          this.menuOpen = !this.menuOpen;
        }
        break;
    }
  }

  onKeyUp(event) {
    switch (event.code) {
      case 'ArrowUp':
      case 'KeyW':
        this.moveForward = false;
        break;

      case 'ArrowLeft':
      case 'KeyA':
        this.moveLeft = false;
        break;

      case 'ArrowDown':
      case 'KeyS':
        this.moveBackward = false;
        break;

      case 'ArrowRight':
      case 'KeyD':
        this.moveRight = false;
        break;
    }
  }

  setInstance() {
    this.instance = new THREE.PerspectiveCamera(50, this.sizes.width / this.sizes.height, 0.1, 5000);
    this.instance.position.set(0, 0, 1000);
    this.scene.add(this.instance);
  }

  setControls() {
    if (TOOLS.isTouchDevice()) {
      this.controls = new MapControls(this.instance, this.canvas);
      this.controls.enableDamping = true;
      this.controls.dampingFactor = 0.05;
      // this.controls.panSpeed = 0.2;
      // this.controls.rotateSpeed = 0.8;
      // this.controls.zoomSpeed = 0.8;
      this.controls.minDistance = 0;
      this.controls.maxDistance = 1000;
      this.controls.maxPolarAngle = Math.PI * 0.5;
      this.controls.target = new THREE.Vector3(0, 0.5, 0);
    } else {
      this.controls = new PointerLockControls(this.instance, this.canvas);

      this.blocker = document.querySelector('.blocker');
      this.instructions = document.querySelector('.blocker__instructions');
      this.dashboardMenu = document.querySelector('.dashboard--menu');
      this.dashboardSound = document.querySelector('.dashboard--sound');

      this.instructions.addEventListener('click', () => {
        this.controls.lock();
      });

      this.controls.addEventListener('lock', () => {
        this.instructions.style.display = 'none';
        this.blocker.style.display = 'none';
        this.dashboardMenu.style.display = 'none';
        this.dashboardSound.style.display = 'none';
      });

      this.controls.addEventListener('unlock', () => {
        this.blocker.style.display = 'block';
        this.instructions.style.display = '';
        this.dashboardMenu.style.display = '';
        this.dashboardSound.style.display = '';
        this.text.closeText();
      });

      this.scene.add(this.controls.getObject());
    }
  }

  resize() {
    this.instance.aspect = this.sizes.width / this.sizes.height;
    this.instance.updateProjectionMatrix();
  }

  update() {
    if (TOOLS.isTouchDevice()) {
      this.controls.update();
    } else {
      // requestAnimationFrame(this.update());

      const time = performance.now();

      if (this.controls.isLocked === true) {
        const delta = (time - this.prevTime) / 1000;

        this.velocity.x -= this.velocity.x * 10.0 * delta;
        this.velocity.z -= this.velocity.z * 10.0 * delta;

        this.velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

        this.direction.z = Number(this.moveForward) - Number(this.moveBackward);
        this.direction.x = Number(this.moveRight) - Number(this.moveLeft);
        this.direction.normalize(); // this ensures consistent movements in all directions

        if (this.moveForward || this.moveBackward) this.velocity.z -= this.direction.z * 400.0 * delta;
        if (this.moveLeft || this.moveRight) this.velocity.x -= this.direction.x * 400.0 * delta;

        // if (onObject === true) {
        //   this.velocity.y = Math.max(0, this.velocity.y);
        //   this.canJump = true;
        // }

        this.controls.moveRight(-this.velocity.x * delta);
        this.controls.moveForward(-this.velocity.z * delta);

        this.controls.getObject().position.y += (this.velocity.y * delta); // new behavior

        if (this.controls.getObject().position.y < 10) {
          this.velocity.y = 0;
          this.controls.getObject().position.y = 10;

          this.canJump = true;
        }
      }
      this.prevTime = time;
    }
  }
}
