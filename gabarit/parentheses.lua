--- A script by Bastien Dumont (with a little modification)
--- from this discussion
--- https://groups.google.com/g/pandoc-discuss/c/PVGvA9h8AOw
local function getLastInnermostElem(elemsList)
  local lastInnermostElem = elemsList[#elemsList]
  if not lastInnermostElem.t == "Str" then
    getLastInnermostElem(lastInnermostElem)
  end
  return lastInnermostElem
end

local function getFirstInnermostElem(elemsList)
  local firstInnermostElem = elemsList[1]
  if not firstInnermostElem.t == "Str" then
    getFirstInnermostElem(firstInnermostElem)
  end
  return firstInnermostElem
end

local function removeTrailingSpace(formattedCitation)
  if formattedCitation[1].t == "Space" then
    formattedCitation:remove(1)
  end
end

local function removeParentheses(formattedCitation)
  local lastInnermostElem = getLastInnermostElem(formattedCitation)
  if lastInnermostElem.text then
    lastInnermostElem.text = string.gsub(lastInnermostElem.text, "%)$", "")
  end
  local firstInnermostElem = getFirstInnermostElem(formattedCitation)
  if firstInnermostElem.text then
    firstInnermostElem.text = string.gsub(firstInnermostElem.text, "^%(", "")
  end
end

local correct_citation = {
  Cite = function(cite)
    removeTrailingSpace(cite.content)
    removeParentheses(cite.content)
    return cite
  end
}

function Note(note)
  return pandoc.walk_inline(note, correct_citation)
end
