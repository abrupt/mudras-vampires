```{=tex}
\cleardoublepage
\begin{nsright}
\thispagestyle{empty}
\vspace*{11\baselineskip}
\emph{c'est une sorte de bouddhisme sombre}

\end{nsright}
\cleardoublepage
```
# Introduction aux mudras

`\textsc{Je suis devenu vampire}`{=tex} par la pratique du *seon*[^1-traite-du-vide-apatride_1]. Mais je n'aurais jamais découvert le *seon* sans mener une existence vampire en Corée.

Je suis devenu vampire par magnétisme. Par nécessité aussi, afin d'exorciser, pour qui en a déjà fait l'expérience, une dépersonnalisation dans la peur panique. Cette peur, comme tous les phénomènes, s'est évanouie, celle pourtant qui vous étreint jusque dans les os. Cette peur et son emprise, je les ai déjà appelées Māra[^1-traite-du-vide-apatride_2].

Je suis devenu vampire par la pratique du *seon*. La méditation *seon* n'a absolument rien à voir avec l'époque, sa lubie. Ce livre n'est pas un livre de développement personnel, ou il l'est, mais à l'usage des vampires. En ce sens, c'est un traité qui s'adresse aux vampires, à celles et ceux en devenir. Il parle du *devenir-vampire* dans la pratique du *seon* coréen. Pratiquer le *seon*, c'est faire l'expérience de l'apatride par l'expérience profonde du souffle. Le vampire parle une langue apatride. La langue apatride se trouve dans toutes les langues, mais ne se reconnaît dans aucune.

Ce livre doit dérouter, il doit décentrer. Pas de raisonnement, de mécanisme rigide, rationnel, mais une dérive dans l'expérience du *devenir*. Les huit *mudras* qui le constituent sont une suite d'états intermédiaires. On les traverse pour en être traversé, comme on l'est en définitive par tout phénomène du langage. Mais ces phénomènes sont des *vues* qui jaillissent dans leurs colorations mentales. Les *vues postulantes* des *mudras* ne disent rien si ce n'est le remuement de la langue. Elles portent des images, les véhiculent depuis les zones d'ombre, celles où se forment les fantômes du sang. Le *Bardo Thödol* décrit les figures archétypiques des états intermédiaires. Certains d'entre nous les ont déjà reconnus dans le rêve ; dans ce rêve qui, à sa manière, grave.

Nous sommes confrontés à une sorte de gestation des peurs, des névroses qui dans l'humain ne demandent qu'à sortir, pour enfin dévisager l'*inhumain* et s'y reconnaître. C'est ce à quoi se résout le ou la chamane, en dévisageant l'*inenvisageable*. La force sensible du monde surnaturel revient toujours aux esprits, suivant l'intuition d'un mystère qui nous parle en silence, par la voix du végétal, de l'animal.

Alors, pourquoi être vampire ? Parce qu'il s'agit avant tout d'être, de parler en apatride, et que le vampire est cet être apatride. Pour aussi sortir de l'humain, de sa dentition inoffensive, plate.

Le mysticisme n'a nul besoin de religion. Le bouddhisme coréen a lui aussi sa part sombre, celle qui relie l'esprit de la montagne, le *Sanshin*, au magnétisme de la terre, au paysage, à sa géomancie. Il parle aux esprits du monde, il parle dans l'œil de l'animal, il parle aussi dans le sang. Le *seon* est une méthode. Il permet de se défaire des purulences mentales. Le *seon* est une attention totale à cet *inhumain* qui se manifeste dans l'humain. Pour le vampire, c'est aussi une méthode d'acuité qui sonde l'intracorporel par le souffle interne, et ainsi le relie au monde. Une force magnétique circule dans le sang, progresse dans le corps, en irrigue les particules, les tissus, imprègne le vivant de toutes les autres vies inhumaines. C'est une unification par la colonne du souffle qui le traverse. D'où cette totémisation que l'on retrouve dans la figure de la *Kundalini*.

Le délire chronique d'un tel traité a ses charmes[^1-traite-du-vide-apatride_3]. Alors, venons-y maintenant, au *traité vampire*, à son ossature en colonne.

Débutons par les *purulences*, ces phénomènes parasitaires du mental qui se manifestent souvent au tout début de la pratique, quand les yeux tremblent encore et que les stimuli nerveux sont enfin reconnus. La première partie du traité les considère dans la pluralité de leurs apparitions. Il ne s'agit pas d'en *traiter* les causes, mais seulement de les reconnaître. Le vampire ne s'occupe pas de psychologie, même s'il n'ignore pas l'existence d'un inconscient collectif[^1-traite-du-vide-apatride_4]. Il en va d'une sorte de combustion, d'une période de *feu de la langue*. Souvent aussi d'hallucinations, quand les résidus de la langue prolifèrent. Le vampire ne les retient pas, crève ainsi la cloche qui les gardait, comme un nid de semences troubles. Les voix parasitaires affluent, s'écoulent comme un liquide purulent.

Dans les premiers temps, l'inconfort de la pratique rebute, car le corps tremble et s'impatiente. Mais le vampire n'abandonne pas. Par la pratique seule, il se détache de ce qui le retenait dans un soi-disant *pour soi* mais qui n'était rien d'autre qu'un amas de phénomènes troubles. Il faut laisser le sang parler, retrouver la cohérence cardiaque, afin de prendre à contre-courant cette volition de l'humain qui est l'un des freins à une *libération* des phénomènes mentaux. L'humain formule ses propres pulsions. Il tisse ses névroses, conditionne son épuisement dans le retour du même[^1-traite-du-vide-apatride_5]. Nous pourrions continuer ainsi, en affirmant que le *seon* est un exercice de régulation de l'énergie libidinale, une recherche du seul équilibre énergétique. Mais la pratique dépasse cette dimension régulatrice, comme il en va d'une libération supérieure, d'une voie de connaissance dans le *devenir*.

L'iconographie bouddhiste use de la représentation de l'île comme d'un lieu inaltérable face aux mouvements et à l'agitation des flots. Le *seon* donne forme à cette île de vigilance. Les *purulences* n'y ont plus de prise, le reflux de l'écume y est repoussé. C'est une solidité sans matière. Le roc est une colonne indémontable, façonnée encore une fois par le souffle. Ce semblant d'immobilité dans la pratique méditative ne doit toutefois pas induire l'idée d'une image morbide du corps. L'arbre de l'île, le bambou noir, est irrigué par la sève. Les minéraux voyagent *invisiblement*. L'esprit du vampire revient toujours au sang, à une cohérence hématique, mais dérivante, suivant la ou les trajectoires de la colonne. À la manière de la silice pour le végétal ou le minéral, le souffle irrigue les tissus organiques, comme il est irrigué en retour. L'oxygène pénètre dans le corps, les poumons, l'abdomen. Cette circulation silencieuse demande la plus grande écoute, car elle n'a pas de vocable. Elle ne peut être dite, si ce n'est dans une langue autre, c'est-à-dire *apatride*.

Les *mudras vampires* sont le cœur du traité. Nous les déclinons en huit *mudras*, comme huit sceaux, à l'image de l'*octologo*[^1-traite-du-vide-apatride_6] scelsien. Si le chiffre annulaire s'enroule indéfiniment, marquant d'un signe le cycle des retours, les *mudras*, quant à eux, marquent les différents états qui se succèdent sur la voie de la *libération*. Dans les Védas, ils sont des guides par la posture, conceptualisant dans chacune des positions des mains les manifestations du divin. Les *mudras* ne sont cependant pas que de simples concepts spirituels, car ils se pratiquent avant tout par le corps, c'est-à-dire par le geste. Il en est de même du langage, tout comme de la méditation *seon*. C'est une pratique. Elle inscrit le corps dans le signe et le signe se réalise ainsi par le corps. Ce dernier devient à son tour terrain du *devenir*, lieu où les énergies circulent, se métamorphosent. Les *mudras vampires* sont la tentative d'inscrire le langage dans une *formule du corps*. Chaque état est formulé en tant que langue *scellée*, même si les sceaux ne seront volontairement pas figurés ici, pour éviter la fixité des figures et que la langue puisse engendrer librement ses mutations. On peut toujours imaginer des mains indistinctes qui, par effets de surimpression, de *morphing*, se métamorphosent en d'autres mains et qui, dans l'absence d'immobilité, n'admettent aucune adhérence.

Venons-en maintenant à la colonne. C'est l'ossature conduite par le souffle. C'est le *canal subtil* qui traverse l'être et qui, en le traversant, lui donne cette possibilité ontologique d'*être* par le mouvement ; par le mouvement des flux qui ne se mesurent pas. L'*être* est dans le *devenir*. C'est ici le sens du *devenir-vampire*. Le *seon* n'est pas une pratique transcendantale comme d'autres pratiques méditatives peuvent l'être. Nous évoquons la *Kundalini* par simple intuition. Le vampire peut y être sensible, si le sang véhicule une mémoire et avec elle les archétypes animaux : reptiles, chauves-souris, mais aussi, mystérieusement, le tamanoir ou *tamandua*, le lémur *colugo*. Ici, la dérive de la conscience --- l'irrationnel si l'on veut --- prend part et devient particules, images issues d'un rêve. Le vampire n'a pas à rougir[^1-traite-du-vide-apatride_7], ces animaux sommeillent en lui depuis toujours, comme d'autres ont vu danser les déités courroucées, formulant la thérianthropie des origines. Mais la digression peut aussi être réfutée, et le traité ose cette réfutation, car au fond *le tantrisme est une farce*, et *les yogis ne sont que des puces*.

Une fois le *feu de la langue* éteint, débarrassé des hantises, des idées et de la pensée discordante, discriminante, le vampire devient vampire dans le *Vide Apatride*. Mais comment parler alors de ce que l'hétérocère blanc *ne pense pas* ?...

```{=tex}
\cleardoublepage
\begin{nsright}
\thispagestyle{empty}
\vspace*{11\baselineskip}

\emph{Tu te fondras alors immédiatement}

\emph{en les divinités buveuses de sang}

\emph{et deviendras Bouddha.}

\vspace*{1\baselineskip}

\emph{Bardo Thödol}

Le Livre tibétain des Morts

\end{nsright}
\cleardoublepage
```
```{=tex}
\begin{chassefixe}

avant d'être vampire ma tête était comme un panier plein de reptiles et de fleurs et de fruits et de vers à l'intérieur et l'un des vers me souriait quand je le regardais et il me demandait de manger les fleurs

je regardais dans les fleurs et je voyais des sexes partout et les sexes avaient des parfums et ils me demandaient de plonger mes yeux et ma bouche à l'intérieur du sexe des fleurs et je me disais que c'était là la seule manière de connaître l'amour que de boire dans le sexe des fleurs

la nuit mon cœur voulait sortir de ma poitrine parce que je voulais être reconnu par tous les êtres et les sexes des fleurs m'avaient fait connaître le désir alors je croyais que le désir était la seule vérité et qu'il fallait mordre la chair et qu'il fallait copuler pour connaître l'amour

je ne savais plus parler et je ne savais plus voir le monde et m'oublier dans le monde alors je me suis assis et j'ai commencé à me taire et alors quelque chose a commencé à parler

\end{chassefixe}
```
```{=tex}
\clearpage
```
### Premier commentaire sur le devenir-vampire

```{=tex}
\hspace{1em}
```
Faire l'expérience de l'apatride, dans la réalisation éprouvée des huit états successifs, ou huit *mudras*.

Que l'acte de *réfutation en soi* conduise à la réalisation des *altérités en soi*, au travers des huit états de pratique.

Que le refuge existe dans l'absence des phénomènes.

Que le mouvement se déplace dans l'annihilation du mouvement.

Que le sang soit la seule réponse au langage.

### Conditions pour l'accomplissement des huit regards

```{=tex}
\hspace{1em}
```
Exercice dans la pratique du *seon*.

*Mudra cosmique*, équilibre total.

Ne rien regarder, sinon l'acuité seule.

# Regard sur les purulences

`\noindent\emph{Purulences, les colères adipeuses, la turgescence mentale.}`{=tex}

`\noindent`{=tex}`\emph{Contamination par les phénomènes, ou le principe des devenirs contradictoires.}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
1.  Le mouvement psychique instable est le mouvement des purulences.

2.  Visages, dents décollées, décollations. Grimaces, hémorragies d'expériences sont les phénomènes de la langue terne.

3.  Le sang pour les buveurs de sang.

4.  Émergence du trouble sensitif, par les mouvements contradictoires, les séquences destructrices comme production du mental.

5.  Période du *feu de la langue*. Foisonnement de la pensée, dans une seule combustion.

6.  Instabilité de la cornée. Les abcès, de déformation en déformation. Catalogue de souillures.

7.  Liens d'asservissement par la dépendance à l'asservissement. Simultanéité des réponses pulsionnelles. *Feu de la langue* terne. Danse des archétypes. Plusieurs visages de démons.

8.  Production des cycles hallucinatoires. Absorption et embrasement.

9.  Le vampire apatride trouvera refuge dans l'absence de phénomènes.

### Objets de la vision

```{=tex}
\hspace{1em}
```
Terreur symptomatique. Projections morbides. L'activité mentale engendre *le même*. Apparition des figures archétypiques de la peur. La mort danse dans la mort.

```{=tex}
\vspace{1\baselineskip}
```
1.  Continuum des constructions mentales. Le trouble adhère au trouble.

2.  La dépendance du mental engendre la dépendance produite par le mental.

3.  Les peurs produisent l'espace qui conditionne l'existence des peurs.

```{=tex}
\vspace{1\baselineskip}
```
*Les figures animales comme figures archétypiques dans le rêve (figures de la purulence).*

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, ne rejette pas ces visions. Elles sont des constructions tout issues du mental. Tu dois les voir comme une aide. Elles sont le visage de ce qui en toi n'a pas de voix..."

```{=tex}
\vspace{1\baselineskip}
```
Figures récurrentes de la purulence : crabes, araignées, bovidés, lézards.

```{=tex}
\vspace{1\baselineskip}
```
"... La rencontre avec l'animal n'aura jamais véritablement lieu. L'animal archétypique, même s'il a des yeux, n'a pas de regard. Il sera une projection rigide, un masque articulé ; un simple objet d'angoisse..."

### Langues de l'animal archétypique

```{=tex}
\hspace{1em}
```
L'animal archétypique possède une langue. Mais sa voix ne peut être entendue.

Le mutisme de l'animal archétypique engendre une multiplicité de langues. La voix de l'animal est une *voix mentale*. Elle se multiplie dans l'espace des phénomènes mentaux.

Dans le rêve, la langue du tamanoir *tamandua* est la langue du mutisme dévorant. Cette langue dévore les interstices vacants du mental.

### Purulences de la mâchoire : images mentales

```{=tex}
\hspace{1em}
```
La mâchoire s'agite plus que de raison. Un resserrement à briser les dents, comme sous l'effet d'une lame.

La purulence dans la mâchoire carnassière. Animaux masticateurs de chairs.

Illusion de la mastication morbide, degré de la sonorité macabre. Les os craquent. Bruit de la déchirure de la chair.

Des yeux ouverts, l'animal halluciné, hanté, il mange la mort. Torpeur de la dévoration, stases du macabre.

Illusions morbides, organes dévorés par un singe. Archétype du corps maladif. *Saturne dévorant un de ses fils*.

Les lézards s'agitent sous le plancher. Reptation de l'angoisse sous-jacente. Les yeux tremblent d'effroi. L'invisible couve des œufs purulents.

```{=tex}
\vspace{1\baselineskip}
```
*Production des cycles hallucinatoires.*

## Absorption

Les purulences s'attachent au mental comme des griffes dans la chair. Elles y laissent leurs marques, s'y harponnent. C'est leur caractère parasitaire. L'emprise de la souffrance engendre de la souffrance. Le mental est contaminé par les masques de la morbidité qu'il conditionne en silence, dans *la chambre noire de la maison du sourd*. Les masques sanglants des animaux archétypiques usent de la sémantique morbide, de la dévoration, du sang, de la bestialité destructrice et mutique. Le mental produit, puis absorbe ses propres images morbides, il les met en branle comme des projections d'ombres et de flammes. C'est l'apparition des démons buveurs de sang sur le *mandala* des divinités courroucées du *Chonyid Bardo*.

Le *tamandua*, acculé par son agresseur, se dressera sur ses pattes arrières, n'hésitera pas à se servir de ses griffes. "Mais, *tamandua*... ces agressions ne sont que des irradiations. Elles viennent de toi. Tu as peur des potentialités inhérentes à ton propre psychisme. Elles proviennent du cercle externe, de l'est, du sud, du centre de ta cervelle de fourmilier."

## Embrasement

*Yi-dams*, attributs de l'horreur : la torsade des crânes s'enroule autour du crâne du *Yi-dam*.

Embrasement : *Pemagargjiwangtschuk*, l'insurpassable danseur du lotus, danse avec les *dakinis* en tambourinant sur des crânes des sons à faire éclater les têtes.

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, ne recule pas, toutes ces choses sont des choses de bien, dans cette lumière, ton corps loin de la lueur terne, est irradié, pendant que les *dakinis*, bleues, vertes, crachent. Toutes ces choses sont des choses de bien... exotisme des fantômes qui touchent tes bras, il pousse des fleurs dans ta peau comme des *mudras*, le geste, de regarder l'espace tout entier, dans cette lumière. *Samaya*... ne recule devant rien, mais il faudrait dire, ne recule pas devant le rien, ne revient pas vers la lueur terne..."

```{=tex}
\vspace{1\baselineskip}
```
"... La lumière, l'as-tu vraiment regardée ? Lumière jaune-rouge, bleu-vert, les huit *ma-mo* terribles, terrifiants, mais pas plus terrifiants que tes propres purulences, la masturbation moite, des potentialités à têtes animales, des totems sexuels, *Bardo-kama*, la mort dans l'âme, mais la lumière... cadavres et squelettes, bouddhisme sanglant, dans la lumière chaude du matin, *seon*, les mains jointes, *seon*, la colonne du souffle... tu portes à ta bouche le souffle, tu fais face aux buveurs de sang, tu reconnais tes propres projections dans le cercle *mandala*, le feu, le rythme, le cercle, le cercle de feu, bouche contre, dans la vision des buveurs de sang embrassant les lèvres crasses des crânes terribles, soupe à la cervelle, la peur rend attentif, l'œil tourne sur lui-même, tourné-retourné et détenteur, vers le centre sans distraction..."

## Feu de la langue

"Noble ami·e, les projections mentales donnent forme aux divinités buveuses de sang. Mais il faut se fondre en elles. Ne perds pas courage, ne recule pas devant la violence des entrailles, des mutilations, des décollations du néant, elles grimacent comme des tortues. Le serpent du *Bardo* se tord, il devient une langue, il devient une écharde. Le tamanoir n'a plus peur du noir, l'obscurité est dans son crâne, non dans la nuit. Parce que l'obscurité est dans les têtes. Avec tes yeux de *colugo* tu fixes la lumière, jusqu'à en être irradié. Tout se consume dans un feu de vacuité. La lumière est un feu de vacuité. Elle brûle sans douleur, sans mot."

"... Le serpent ouvre des yeux de vacuité, le volcan qui crache de la bouche de *Salanäpa* est un puits de vacuité. La lumière est son principe fondamental. Les démons s'agitent parce qu'ils vont bientôt disparaître dans le feu. Le vide consume la violence des phénomènes..."

```{=tex}
\clearpage
```
```{=tex}
\begin{chassefixe}
je voulais mordre tous les êtres autour de moi et je tombais de sommeil et je voulais tout le temps dormir et à chaque instant de ma vie je voulais dormir

je n'arrivais pas à m'endormir car les battements de mon cœur me réveillaient et j'avais l'impression que mon cœur d'un seul coup s'arrêtait de battre alors je voulais parler mais je n'arrivais pas à parler

je voulais mordre tous les êtres car quelque chose d'invisible me mordait moi aussi et je rêvais de tortues et je voulais être aussi calme que les tortues de mon rêve
\end{chassefixe}
```
```{=tex}
\clearpage
```
### Principe des devenirs contradictoires

i.  Instabilité vibrionnante du mental contaminé

ii. Illusions irradiantes

iii. Irrationalité des pensées discriminantes

```{=tex}
\vspace{1\baselineskip}
```
i.  L'illusion naît, puis meurt. Il en est de même de tous les phénomènes. La peur crée de l'aversion pour la peur. Cette peur est une erreur, une contradiction. Elle aimante les phénomènes de l'*incomposé*. Son activité est un trouble qui vibrionne. Dépourvue de sens, elle alimente la contradiction.

ii. L'Ami du soleil a déclaré\
    Que les formes sont pareilles à l'écume,\
    Les sensations à une bulle,\
    Les discriminations à un mirage,\
    Les formations à un bananier,\
    Les consciences à une illusion[^1-traite-du-vide-apatride_8].

iii. Les mondes des représentations n'étant plus composés que de pensées discriminantes et illusoires, les perceptions contradictoires produisent un essoufflement psychique, une saturation de visions marquées par la confusion. Les formations purulentes maintiennent l'esprit dans un ferment de mensonges et d'illusions. Les devenirs prennent alors le visage de l'*envisageable*, *du même* toujours identique qui contamine dans la peur.

### Deuxième commentaire sur le devenir-vampire

```{=tex}
\hspace{1em}
```
La pratique du *seon* permet d'établir un processus de connaissance, celui-ci passant par l'expérience et par la perception des états intermédiaires, puis par l'édification de la *colonne*. C'est à travers cet exercice que la *libération* est possible et que le *devenir* est débarrassé de ses contradictions, ou des alluvions de purulences.

Le devenir est le *devenir-vampire*.

```{=tex}
\vspace{1\baselineskip}
```
`\noindent\myrulefill Méditation Vampire`{=tex}

`\noindent Méthode du \emph{Seon} Vampire\myrulefill`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
`\itshape`{=tex}

Assis·e face à un mur. Tes yeux de vampire sont mi-clos. Tu ne regardes rien. Ton champ de vision est ouvert.

Tes genoux de vampire sont plus bas que ton bassin et tes jambes dans la position dite du demi-Lotus.

Ta colonne vertébrale de vampire est droite.

Ta tête de vampire est légèrement penchée vers l'avant.

Ta main droite de vampire est posée ouverte sur la paume de ta main gauche.

Tes deux pouces de vampire se touchent.

Tous tes muscles de vampire sont détendus, aucune tension ne doit traverser ton corps.

Tes bras de vampire sont relâchés, tes mains sont posées au niveau de l'aine, du pubis.

Vampire, tu te concentres sur ton souffle. Ta respiration est le centre du `\emph{seon}`{=tex}, sa colonne. Ta respiration est abdominale.

L'attention est le deuxième centre du `\emph{seon}`{=tex}. Tu ne dois à aucun moment t'assoupir. Le `\emph{seon}`{=tex} vampire n'a rien d'une posture passive ou amorphe.

Le `\emph{seon}`{=tex} est une expérience de l'acuité. L'acuité seule rend possible l'expérience de la vacuité.

```{=tex}
\normalfont
\clearpage
```
```{=tex}
\begin{chassefixe}
je voulais voir couler le sang dans les choses

je voulais voir ce qui dans l'œuf n'en finissait pas de remuer

il y avait dans l'œuf tous les yeux du monde et toutes les langues du monde

et la serre \emph{Walipini} était un œuf et nous étions deux vampires et nous avons décidé de vivre comme deux vampires dans la serre \emph{Walipini}

l'agave poussait dans notre serre et je regardais dans tes yeux les feuilles d'agave qui poussaient dans la serre

le silence dans tes yeux c'était celui des feuilles d'agave et le silence des feuilles d'agave c'était celui de la serre \emph{Walipini}

nous sommes devenus deux vampires silencieux au milieu des feuilles d'agave de la serre \emph{Walipini}
\end{chassefixe}
```
# Les états intermédiaires

*Dans l'observation des perceptions, le mouvement apaisé.*

`\noindent`{=tex}`\emph{Les passages, les vues postulantes dans la vacuité.}`{=tex}

## Perceptions du mouvement

1.  Appréhensions des états intermédiaires

2.  L'indéfinissable rassemblé

```{=tex}
\vspace{1\baselineskip}
```
1.  Les figures purulentes glissent sur la cornée, entièrement *incomposées*. Elles sont nécessaires, permettant d'appréhender le mouvement des phénomènes.\
    "Noble ami·e, l'exercice est cérébral, pas de mensonge. La question de la mort... avalée spirituellement, mâchée comme du sang. Concentre-toi sur tes propres illusions, elles ne sont que des phénomènes d'interdépendance..."\
    "... Alors il faut le courage, pour tuer en soi les purulences, celles qui vous attachent, vous lient, à ce que vous croyiez être faussement le soi le plus vrai, le plus profond, mais qui n'était en définitive que mauvais reflet, pulsions morbides, amour déphasé, ou phagocytage de la psyché..."\
    "... Doigts de monstre, c'est Māra jouisseur de la mort, ses lèvres roses, toujours prêt à la basse besogne. Mais ne t'en prends qu'à toi-même, à tes propres projections mentales. Il vient de toi et d'aucun·e autre. Les projections, celles qui guident tes doigts, celles qui dressent les membres, comme la peur du vide, sont un état intermédiaire : *Bardo*. Tu vas renaître cent fois... une vie de lama ? Bien loin, n'y songe même pas..."\

2.  "Noble ami·e, place dans ton esprit, disons... une nouvelle langue, sans alphabet. Alors, observe la lettre de l'absence, et dans ton esprit élabore une construction, qui sera une langue de l'absence de langue."\
    "... Tu peux parler dans la langue absente qui se transforme et qui ne sera jamais la même, dans les mutations du moi..."\
    "... La dialectique du Vide participe aussi à la destruction des purulences."\
    "De l'absence naît l'apaisement. De l'apaisement naît l'absence..."\

## Les passages

*Établir sa position pour disparaître.*

`\noindent`{=tex}`\emph{Disparaître alors pour s'apparaître à soi-même.}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
`\noindent`{=tex}

```{=tex}
{\begin{nscenter}\notosymbols ◖ ◗\end{nscenter}}
```
```{=tex}
\vspace{1\baselineskip}
```
Les états intermédiaires sont des seuils. Ils relèvent d'une succession d'expériences. Le cheminement dans les états intermédiaires engendre un mouvement. Ce mouvement peut naître dans l'immobilité du *seon*.

Le *seon* permet la perception des mouvements de l'esprit. Les mouvements de l'esprit circuleront alors, donnant forme aux différents états intermédiaires.

## Les vues postulantes

Les états intermédiaires ne tiennent *lieux et espaces* de visions (*vues postulantes*) que par les mouvements de l'esprit.

```{=tex}
\vspace{1\baselineskip}
```
**Réfutation : L'esprit n'a pas de nature propre.**

```{=tex}
\vspace{1\baselineskip}
```
`\uline{Ces mouvements sont des manifestations d'un \emph{devenir} sans forme.}`{=tex}

Les métamorphoses qui le hantent sont des formations sans nature propre.

Le sang et sa circulation, le battement cardiaque, son rythme, en sont la pulsation primitive.

L'origine, le centre des figures archétypiques serait le *tellurisme cardiaque*.

La circulation, la vie hématique engendre le kaléidoscope des figures archétypiques.

Le totem naît du sang. Les figures du rêve naissent dans le sang.

Le *devenir-vampire* se réalise dans le sang.

L'intracorporel engendre les *vues postulantes*. Le totem intracorporel, ses figures archétypiques doivent être vues comme des *postulats du sang*.

Les *vues postulantes* sont une lecture du sang.

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, tu tenteras cette introspection dans et par le sang. Tu tenteras l'introspection hématique du vampire..."

### Troisième commentaire sur le devenir-vampire

```{=tex}
\hspace{1em}
```
Les huit états intermédiaires sont traversés, produisant les huit *vues postulantes*, recueillies ici sous la forme de huit *mudras*.

Les huit *mudras* citées ci-après sont des fragments des *vues postulantes*. Ils ne sont que des états mettant en lumière les mouvements de production de la parole, dans le raisonnement des *huit extrêmes*.

Chaque *mudra* développera les degrés d'expérience des *vues postulantes*.

# Mudras Vampires

## Première mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Walipini

"Noble ami·e, oublie, tu veux bien, le macabre, l'image macabre, car les os sont aussi l'ornement, sont aussi la fleur et l'ornement..."

"... Il n'y a pas à penser, à tergiverser pour une noix, une araignée. Il n'y a plus d'angoisse, de terreur, de crabes rouges qui voudraient te caresser les yeux. Les démons sont venus te gratter l'oreille, te chatouiller les pieds, mais c'est fini. Tu aurais préféré que l'on te grignote l'oreille ? Que l'on te grignote le cœur ?..."

"... Le végétal ne ment pas, débarrasse-toi des résidus de conscience..."

"... Tes os sont devenus du lait, des champignons bleus sur du chanvre brûlé. L'animal est sevré, il respire enfin, aspire à une vie poreuse, sur le globe crevé de tes yeux..."

"... *Trsyati-Walipini*, pose ta dent sur le sol, fais la tourner, que ta dent décide de celui ou celle que tu vas mordre..."

"... Les pagodes sont pleines de vide et de nuées, de chauves-souris transformées..."

"... Écoute la très absurde poésie des lémuriens..."

"... Dans tes rêves les squelettes --- exosquelettes prient, leurs bras zébrés d'araignées. À ce jeu, parfois tu gagnes, parfois tu perds, comme le *monk*, comme le singe-invertébré, comme l'insecte dégénéré de la constellation du lièvre..."

"... Tu étais *Tani* sous le bananier. Tu étais *Palestine* et tu ne voulais plus quitter cet arbre, car pour toi toutes les têtes étaient moisies..."

"... Ils ajoutent de la guerre à la guerre, et s'ils pouvaient vomir avec leurs yeux, ils le feraient. Dans leur bouche, la violence est aussi logique que de la salive sur des muqueuses. C'est pour cela que tu as choisi la serre, c'est pour cela que tu as choisi les palmiers, les célosies et l'agave..."

"... Le lémur noir a des yeux de pierre en *jade lémurien* noir..."

"... La vie silencieuse s'exprime surnaturellement. Il faut imaginer l'arborescence, les ramifications invisibles, comme sur une carapace de tortue les motifs sont gravés selon le rêve. Car le rêve grave, tes rêves sont des rêves indigènes, des rêves de vampire..."

"... Il est difficile d'accepter cette condition --- être vampire --- condition qui te pousse à justifier cette dent contre le monde..."

## Deuxième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Tani

"Noble ami·e, ils peuvent parler, à ce moment-là, il n'y aura que du phosphore..."

"... Les figuiers de barbarie abritent des visages. Peux-tu voir des visages dans les feuilles d'agave ?..."

"... Un monde qui ne mérite pas sa faune est un monde à déflorer..."

"... Est-ce que ça les amuse encore, la décapitation des cachalots ? Est-ce que ça les amuse encore, la décollation des cétacés ?..."

"... Les crânes se multiplient. En dehors de toute logique, le monde continue de se transformer, comme le futur vient en premier, et le passé, après..."

"... C'est un crâne dans ton crâne qui, sorti du néant, ne veut plus se taire. La vie maintenant, ce n'est plus se montrer, ce n'est plus montrer délibérément sa face de rat..."

"... Les grands bananiers remuent, il faut avoir l'œil pour voir le mouvement infime dans les feuilles de bananier. Pour voir aussi ta langue remuer, il faut avoir l'œil, pour voir ta langue qui remue dans ta bouche de vampire..."

"... Lémurien fantôme, tes dents sont des feux de forêt, des *jades lémuriens* noirs..."

"... *Tani* --- *Tani Pales Tina*, tes oreilles remuent dans les fougères. Les sauterelles sautent, elles sautent de fougère en fougère. L'hétérocère blanc est un fantôme. Il est sorti de ton esprit..."

"... Colonne, mais colonne spirituelle, c'est mieux que des glandes, des glandes sexuelles. La vieille *mudang*[^1-traite-du-vide-apatride_9] pleure des larmes sèches. Les esprits de la nuit l'ignorent, ils préfèrent caresser des coléoptères ou dialoguer avec les opossums..."

"... Mange un peu de melon jaune, mange un peu de melon rouge. Le fruit a mûri, dans leur cervelle le fruit a poussé dans la nuit..."

"... Tu pries pour les opossums, tu pries pour les fruits posés sur le sol, le chien à trois yeux a la langue qui pend, ses yeux louchent, il pisse dans les fougères. Il veut croquer, il veut mâcher une famille entière d'opossums. Saloperie de chien, de près tu ressembles trop à un humain..."

"... Sur ces visages, *mudang mudang*, sur ces visages, des yeux dégoulinent ; des yeux batraciens. Tu vois la variole dans leur sourire. Mais tes yeux vampires poussent comme des bégonias..."

"... Chauve-souris mignonne, viens par ici, je mettrais ta tête dans ma bouche..."

## Troisième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Kundalini

"Noble ami·e, les chauves-souris s'animent, les chauves-souris s'aiment dans tes cheveux. Il n'y a que le souffle, que veux-tu que je te dise d'autre ?..."

"... J'ai des yeux dans les yeux, j'ai aussi une bouche dans la bouche. Tu le sais, les deux serpents *Kundalini* sont dans le souffle qui te traverse, mais ce qui me rend fou, c'est le silence dans tes yeux..."

"... Ils ont repris la *Kundalini*, symbole morbido-tantrique. Tu me demandes pourquoi ce vaisseau sanguin lui sort du nombril, à cette figure de damné, dans cet enfer du bouddhisme..."

"... Tu vas penser à partir de la non-pensée, une logique de silence. Tu vas nouer les herbes tout en restant sauvage..."

"... Les ombres prennent la forme de lémuriens noirs. Les crabes dans ton rêve ont un sourire de tigre. Les crabes dans ton rêve ont un sourire lubrique. Des yeux noirs et ronds que l'on brûle, parce que le monde est devenu un enfer, jusque dans la gélose..."

"... L'humain n'a pas fini de se plaindre de l'humain. Son ego le ronge comme la cysticercose. La décision de vomir, chez l'humain, est de tout tapisser. Ils ont chevauché le cheval blanc ? Baisé la croupe brûlante qui gémit, coïté avec passion comme des poissons rouges..."

"... *Contrapunctus*, tes dents, *contrapunctus*, et ton souffle, *contrapunctus* qui passe, *contrapunctus*, entre tes dents... Tu le dis clairement, c'est comme s'il y avait une chose ou deux qui s'ouvrent *clairement* dans ton crâne de vampire..."

"... La société angoisse dans son développement personnel. Elle veut méditer, mais a la tête pleine de vers. C'est neurologique, égocentriquement plié sur du mensonge. C'est glacial, le sourire, la gencive du gourou morbide. La chamane sort de son trou, la vieille *mudang* rapplique..."

"... Je, il n'y a plus de je... suis un imbécile, je palpe des plantes comme un imbécile, et comme d'autres touchent des zones érogènes... je ne masturbe pas le végétal en jouissant comme une couleuvre..."

## Quatrième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Colugo

"Noble ami·e, si tu veux faire capituler le démon, tu ne lui couperas pas la tête, simplement tu lui montreras tes ongles, tu lui montreras aussi que ta mâchoire est souple comme une feuille de bananier. Tu lui parleras, sans parler, des *colugos*, roses comme des ongles, et des primates dans les palmiers..."

"... Finis tes phrases, *colugo*, tu es là à aligner deux, trois mots, c'est dommage. La *mudang* est hystérique, elle crache sur les tapis de fleurs..."

"... L'ontologie est une carie qui donne l'impression d'avoir une dent, alors que pendant ce temps, les démons pincent des ovaires. Ils ont cru qu'on allait t'attraper dans une situation aussi crasse ? Pas d'hagiographie pour un vampire comme toi..."

"... Mais, mon ami·e, ne montre pas ainsi double, triple visage confit. Si je te parle, mes os te parlent, et mon œil te parle aussi, jusque dans le sang, je te parle..."

"... Au bout de la perche, je ferai un pas de plus..."

"... On a craché sur le sol, en souvenir des os du serpent, en souvenir du serpent osseux, en souvenir des yeux sans paupière du serpent..."

"... Les lys tigrés s'ouvrent sur des tiges tigrées... la poésie, c'est la crème de la crème, pour une grande crémation, regarde bien sa combustion, par les flammes, le reliquaire gigogne contient un bol de terre, qui lui ne contient rien..."

"... Ils font des démons des figures grossières, que l'on agite comme des visages grimaçants, édentés, décollés... Regarde encore le bûcher des couleuvres colorées..."

"... Ils veulent la pluie ? ils appellent la *mudang*... Ils veulent un enfant ? ils appellent la *mudang*... Ils veulent parler aux morts ? ils appellent la *mudang*... Ils veulent plus d'argent ? ils appellent la *mudang*... Ils veulent la *mudang* ? ils appellent la *mudang*..."

"... Ils ont peur de la femme-renard, ils ont peur d'être émasculés, ils ont peur de perdre en quantité, de perdre leur membre. Ils ont peur de perdre le pouvoir, de ne plus être des mâles, de perdre leur organe dans le feu de leurs poils..."

"... Vampire, que retiens-tu de l'Histoire ?..."

## Cinquième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Fayoum

"Noble ami·e, tu peux donc pratiquer *seon*, en laissant la folie de l'Histoire pourrir dans sa folie. Tu pratiques *seon* dans la serre, tu pratiques *seon* pour les os et pour les yeux des lémuriens..."

"... L'être éveillé a vu un sourire dans le ver déchiqueté, il a vu un sourire dans le ver déchiré qui gesticulait sur le sol..."

"... Pour atteindre l'éveil, ils n'hésitent pas à se couper un doigt, à couper un chien en deux, en quatre, une chienne en gestation... ils n'hésitent pas à se pendre à un arbre avec la bouche..."

"... Pas besoin de se donner tant de mal, pour les polypes, pour le tigre tanné en peau de tapis colonial..."

"... L'orchidée souterraine est blanche comme les ailes des hétérocères, avec son air de *Brahmaea*, avec son air de vampire..."

"... Si tu me mords, je te demanderai de continuer... entre mes deux yeux, il y a tes yeux de vampire... Tous les deux, on ne parle que de photosynthèse..."

"... L'oasis est intracorporelle..."

"... Comme la cercosporiose jaune et noire, le sang a coulé de leur bouche... c'était à se tordre, de mondanité en mondanité, ça sentait de la bouche, l'asticotage pour les asticots, c'était très sage, c'était très beau, mais je préfère manger avec toi des abricots du Fayoum..."

"... Tes yeux ont la profondeur de la chlorophylle, mais qui thésaurise ici ? La plus belle des magies noires, c'est la photosynthèse..."

"... Leurs yeux ne sont pas de l'ombre, leurs yeux sont de la décomposition... leurs yeux sont la décomposition de leurs yeux..."

"... Tu vois bien, je n'abandonne pas ma folie, je garde mon sourire de lémurien..."

"... Les lémuriens n'ont plus parlé de rien, les singes hurleurs eux ont murmuré contre les murs... bien que nous, vampires, nous n'ayons jamais menti..."

## Sixième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Photosynthèse

"Noble ami·e, quelques chauves-souris en vol désordonné s'échappent parfois de la serre, comme une langue qui sortirait d'une bouche..."

"... La serre, c'est ton crâne... Dans la serre s'élèvera la colonne..."

"... Racines, statocytes polarisés, essentiels pour la photosynthèse, le gravitropisme et son mystère, mais alors ?..."

"... Le serpent a une tête de rat, ses écailles ressemblent à des plumes... Les chauves-souris sont nouées, elles sont nouées comme des nœuds, et un filet de sang noir s'évapore..."

"... Sous la lune, nous regardons encore la constellation du vampire... ami·e vampire, dans cette vie, j'ai mis mes yeux, j'y ai mis aussi mon âme amie..."

"... Les têtes des cactus brûlent la chlorophylle. La perlite crépite dans le substrat des flammes. Les cactus cierges sont aussi beaux que des poumons..."

"... Le souffle porte en lui les mutations du vivant. Les courants silencieux de ton esprit, je les sens, je te jure... tu es un vampire parfaitement tangible..."

"... La cervelle est l'animal à ventouse, il s'enraye pour un rien, pour une histoire de fessée. On porte le poids de la culbute, de la médicamentée... je ne parle pas pour le patriarcat morbide, la nuit est déjà assez verte, ils ont encore cette envie irrépressible de se montrer les dents, de se montrer la moue... les lémuriens horrifiés leur lancent des boisseaux, des livrées de branches en réseau ramifié, des nattes tressées..."

"... Je t'en supplie, mon vampire adoré, oublions leurs manières stupides, veux-tu que nous goûtions encore à la joie du sang ?..."

"... Tu veux parler du sang, ils n'ont pas d'yeux, leur pensée symptomatique éponge la salmonelle... Ouvrez-moi le ventre, cherchez-y des organes : vous trouverez du *jade lémurien*, des nudibranches, des cactus ou des fleurs de bégonias..."

"... Ils font main basse sur les feuilles d'agave, ils sont nombreux à pleurer, leur crâne farci, nourri, gelé, le garrot du moi-moi-moi..."

"... Vois-tu le vice dans le lit ? tout le monde dit : j'écris, mais personne ne dit : je vis. Ils cultivent l'ego, d'aucuns, d'aucunes massent la bosse, ils iront chercher dans les buissons des moules, des poils de mustélidés..."

"... La coupe est pleine de sang, dans le cône remue un remuement très spécial... donc pas de mezcal, de *coconut latte*..."

"... Réveille-moi, j'ai besoin de tes *mudras*, plus belles que des déités perforées... Aujourd'hui, tu as hésité, tu as voulu détruire la serre *Walipini*. Objection : on ne peut détruire ce qui n'est pas..."

« ... Analyse la formation de ce qui n'a pas de nature propre, ce que l'on nomme vacuité, ou ce qui est dépourvu d'entité réelle,

--- le vide, comme principe et condition de transformation du réel,

*uk uk uk*... »

"... ils se mettent à baiser des crânes, à danser en baisant des crânes, et pendant ce temps, leur crâne, qui le baise ? Pendant qu'il remue ?..."

"... *P'alttak-p'alttak*, ils placent des crânes en équilibre sur leur crâne... c'est mignon tous ces yeux noirs infâmes qui tournent de l'œil, prêts à s'écrouler, à se briser..."

"... L'*imago* a débuté, l'*imago* ne finit jamais... fin des temps, *amigo imago*, ferme la bouche, n'en dis pas trop..."

## Septième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Sisal

"... Toutes les couleurs, toutes les langues parlent dans le serpent *Kundalini*, et tu n'as pas besoin de parler, ni de te reproduire dans un bruit de boue. Tu n'as plus besoin de collectionner des crânes, de recueillir le spermaceti dans un bol troué..."

"... Les bananiers ont fleuri parce que tu l'as voulu, les fleurs d'agave ont fleuri parce que tu l'as voulu aussi... Tes cils de vampire sont des agaves en fleur..."

"... À la lumière du jour, tu verras que le vide retourne la terre, comme les vers de terre tournent, retournent la terre, mais jamais vraiment la même terre... je dis que tout change, c'est déjà dit, et que rien ne meurt... palpitations, je dis encore, tachycardie, alors qu'il suffit de respirer et de trouver encore la cohérence cardiaque..."

"... *Maguey cabuya*, pita rosette épaisse, ils courent comme des fous, des folles, même s'ils n'ont pas de pieds, même s'ils n'ont pas de fesses..."

"... Il faut que tu joignes les deux mains pour que la colonne, *mudra*, se vide, *mudra*, et se remplisse de vide... Leurs mécanismes spirituels sont l'adoration du néfaste... Embrasse-moi, *Arūpaloka*, que nos langues soient une langue... embrasse-moi, *Arūpaloka*, jusqu'à la fin des périodes cosmiques, dans le triple monde réduit en cendres..."

"... *En réfutant l'existence des causes de l'attachement et de l'aversion, le déplaisant est désigné. Si le désagréable n'existe pas, comment l'aversion existera-t-elle ?*..."

"... L'aversion s'évapore comme de l'urine dans tes reins. Tu ne croiras plus : les petits mensonges, le macabre et sa torsion, sa musique libidinale... c'est fini, car ils prendront feu dans l'infinitésimal..."

"... Mets ton cœur dans mon cœur, avec les six connaissances, mets ton cœur et tout ton être dans la serre *Walipini*... tous deux composés, nous sommes incomposés..."

## Huitième mudra `{\notosymbols\scriptsize\raisebox{-0.0ex}{●}}`{=tex} Kulung kulung

"... Il y a un crâne dans la montagne, il est gros comme cent crânes, il est grand comme trois fois trois cents crânes... La fin d'une période cosmique correspond à la maturation d'un fruit dans la serre *Walipini*..."

"... Touche le tambour de la peau, *skin drum*, tout est phénomène..."

"... L'orage est dans les cervelles..."

"... *Tani*, je parle seul, mais si je parle parfois, je parle aux fantômes..."

"... S'ils viennent avec des mains de voleurs, alors il n'y aura rien à voler..."

"... *Mudras* comme on plie les doigts, mais ils ne savaient pas, ils n'ont jamais su que la serre était en cire d'abeille..."

"... Je raconte très bien des histoires de crânes, des histoires où tout est bien qui ne finit jamais..."

"... Veux-tu démêler le vrai du faux, ou le faux du vrai ? Veux-tu enfin vivre en paix, dans un monument sans forme, dans le monument de tes poumons ? Sans les subterfuges, les vermifuges câlins... J'ai du sang dans la bouche, un goût de sang, un goût sanguinaire pour le sanguinolent..."

"... Je vais dire que j'ouvre, simplement, que j'ouvre pour toi mon équateur mental, que je l'ouvre pour toi, que j'ouvre une jungle, que j'ouvre une serre utopique, comme un mythe surgit dans le monde, entre deux mondes, entre mes dents de lémurien, l'écorce d'un fruit rouge s'ouvre, sur un fruit rouge sang..."

"... Dans ton équateur mental, le fruit organe grignoté, *kulung kulung*, dégouline de mes dents et c'est ainsi, c'est probablement ainsi *Tani Tani*, que le végétal progresse, qu'il progresse dans ton esprit... que ta langue aussi touche ma langue, *Tani*, c'est cela... la naissance du goût vampire..."

« ... Dans notre esprit il y a le *Vide Apatride*, il n'y a pas de lyrisme au final, de démence, c'est bientôt la fin, *Tani*, ton souffle descend, c'était prévisible, dans ma gorge... tu me dis que la chauve-souris était déjà dans mes poumons, tu me dis que le vampire était déjà dans mes poumons... »

"... Le mythe est visible, ça ne fait pas de doute, dans la nervure des feuilles... il reste du fantôme en toi, *Tani*, une langue animale, une langue apatride, un démon des plantes, un démon de l'impermanence et du vide, qui n'existe dans aucun des phénomènes de ce monde..."

"... C'est la fin des catégories, de l'animisme, de la poésie des crocodiles... le cœur est une éponge, il prend tout, il brûle comme une feuille, mais ne refuse pas ce cœur qui ne refuse rien..."

"... Les fantômes sont nés sous la chlorophylle..."

« ... L'interpénétration des deux serpents qui s'enroulent autour d'une colonne en mouvement produit le troisième serpent dont l'unité est engendrée par la montée hélicoïdale... »

"... etc., etc."

# Formation de la colonne

`\noindent`{=tex}`\emph{Intuition fondamentale, la Kundalini.}`{=tex}

`\noindent`{=tex}`\emph{Les animaux qui forment la colonne.}`{=tex}

`\noindent`{=tex}`\emph{Le totem intracorporel véhiculé par le souffle.}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
`\emph{\uline{Une fois les huit états appréhendés, la langue tue.}}`{=tex}

`\emph{\uline{L'intuition, une macération des éléments du langage.}}`{=tex}

`\emph{\uline{Le regard tourné vers une vie surnaturelle, intracorporelle.}}`{=tex}

`\emph{\uline{Une langue primitive, non encore formulée, parce qu'elle-même foyer des formules et des mutations.}}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
**Réfutation : Seule la Vacuité élève au *devenir*. Les images, les apparitions ne sont que des illusions, des substrats fragiles du mental.**

```{=tex}
\vspace{1\baselineskip}
```
`\emph{\uline{La colonne \emph{Kundalini}, foyer des mutations, irriguée par le sang.}}`{=tex}

`\emph{\uline{Le souffle comme élément fondamental et flux des transformations.}}`{=tex}

`\emph{\uline{La cohérence cardiaque, comme pulsion primitive des révélations.}}`{=tex}

`\emph{\uline{Les formes de vies surnaturelles produites dans la montée verticale, le souffle et le sang comme principes fondamentaux des apparitions.}}`{=tex}

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, merveilleux vampire de l'intuition, tu devines ce qui dans l'invisible agit comme une mémoire hématique, et dont le foyer serait *animé* par le sang, celui-ci drainé par le souffle. Cette colonne de sang est spirituelle, car peuplée par les animaux du totem intracorporel..."

```{=tex}
\vspace{1\baselineskip}
```
*Colonne des serpents de sang, la torsade de la colonne caducée, la langue hématique serpente, le souffle irrigue le sang des animaux.*

*Le vampire de l'intuition tourne son regard sur la nuit des apparitions intracorporelles.*

*Tortue noire, près du nombril le sang, le foyer des transformations.*

```{=tex}
\clearpage

\begin{nscenter}

\vspace*{2\baselineskip}

{\notosymbols ◯}

\vspace{1\baselineskip}

COLUGO

esprit de la colonne

SERPENT

du nerf opaque

TAMANDUA

langue des cavités

LÉMUR

du \emph{jade lémurien}

TORTUE NOIRE

de calcaire

sur le caducée

FANTÔME

la main d'écailles

la photosynthèse du sang
\end{nscenter}
```
```{=tex}
\clearpage
```
les deux serpents KUNDALINI et la tortue de rivière 자라

séjournent dans l'être INVISIBLE

dans la charge du souffle des poumons jusqu'au SANG

la mémoire est irriguée

```{=tex}
\vspace{1\baselineskip}
```
l'oxygénation du SANG

fait briller leurs yeux noirs

dans la NUIT DU CORPS

```{=tex}
\vspace{1\baselineskip}
```
Le *seon* est une voie vers le *devenir-vampire*.

La colonne est avant tout une colonne du souffle.

## Révélations du crâne

*Le crâne a eu son mot à dire, avant qu'il ne se...*

```{=tex}
\vspace{1\baselineskip}
```
"... je dirais, un tellurisme, ou un force invisible, bien que le monde soit devenu aveugle, et que personne ne semble voir le magnétisme de la pierre. Mais, en vampire, je la vois bien, elle est là, dans la matière, comme une vie qui parle une autre langue, à l'intérieur. C'est encore une forme de silence, une forme de vide, une vibration secrète, peut-être invisible, mais révélée par cette invisibilité même au sein du visible."

"... la pratique du *seon* vous amène à connaître le vide dans l'expérience... En vampire, vous vous détachez des pensées discriminantes, des états mentaux liés à l'interdépendance des phénomènes..."

"... je me retrouve là, chaque jour face à la fenêtre, assis en silence dans la même posture. Les voix qui tournent finissent alors par s'évanouir. Il n'en reste rien, de ces choses dont il faut se détacher, des pièges, des ruminations mentales... Le *seon* vous en libère. Vampire enfin, vous pouvez ne plus penser avec des mots..."

"... alors ce fantôme qui n'a rien à dire, et qui parle en moi, s'épuise en moi, comme le moi s'épuise, fragmenté, puisqu'il n'y a pas d'ātman ; ces corps ne sont que des résidus de purulences, peurs, angoisses cristallisées en peaux moites..."

"... voici un souvenir d'enfance, et peut-être les prémices de ma vie de vampire... Enfant hépatique, la médecine n'apportait aucune guérison, aucun remède, comme si j'étais invisible... Et puis mes parents décidèrent de se tourner vers un sorcier. Je me souviens encore aujourd'hui... sa cuisine, l'immeuble, les grands bocaux de plantes à infuser, les feuilles longues et ondulées qui devaient insuffler leurs secrets pour me ramener à la vie. La mémoire du végétal fut ainsi donnée comme remède à la mémoire du sang..."

"... alors le souffle vous traverse comme une colonne, pacifie les tensions, les nerfs, le sang... Il irrigue, encore, donne une cohérence à la circulation des flux de pensée, des flux sanguins. Il accorde les phénomènes à l'invisible, à son énergie qui agit dans l'expansion insaisissable des forces, du magnétisme... C'est le vide qui circule en vous, fait de vous un vampire dans le vide... Vous devenez alors vampire dans la vacuité..."

```{=tex}
\begin{chassefixe}
j'ai compris que mes os n'étaient pas moi et que je n'étais pas mes os et que dans mes os parlaient d'autres voix qui n'étaient pas ma voix

j'ai commencé à parler en vampire et alors j'ai commencé à parler sans parler

j'ai commencé à te parler sans parler et tu m'as compris et alors j'ai compris que toi aussi tu étais vampire
\end{chassefixe}
```
## Esprit de la colonne

```{=tex}
\vspace{-1\baselineskip}
\hspace{1em}
```
*fluidifier la structure osseuse*

*EN ESPRIT et par le souffle*

*l'esprit n'est pas véhiculé par la matière*

*c'est l'esprit qui véhicule la matière par le souffle*

*l'esprit accompagne verticalement le souffle primitif*

```{=tex}
\vspace{1\baselineskip}
```
LA VOIX PARASITAIRE SE TAIT

ET LA PENSÉE N'A PLUS LIEU D'ÊTRE

QUAND SUBSISTE SEUL

PLUS QUE LA PENSÉE

LE VÉHICULE DE LA PENSÉE

```{=tex}
\vspace{1\baselineskip}
```
*les peurs ne sont que des formations de sel*

*les images mentales s'évaporent sans laisser de traces*

```{=tex}
\vspace{1\baselineskip}
```
Le kaléidoscope est un mensonge des nerfs.

Le vampire ne cesse de se traverser lui-même.

```{=tex}
\vspace{1\baselineskip}
```
**Réfutation : La *Kundalini* n'est ni animale ni magnétique. Elle est apatride.**

```{=tex}
\vspace{1\baselineskip}
```
Ses figures mutent, elles n'ont pas de formes définissables. Telles des flammes, elles se transforment. Seul le magnétisme énergétique les alimente, les *anime*.

C'est un tellurisme des os, de la colonne vertébrale.

Les animaux archétypiques ne sont que des figures de compréhension, des repères mentaux. Elles proviennent d'une sémantique, ou d'un langage. La *Kundalini* n'a pas de langue.

La *Kundalini* se meut dans sa mutation. Les contorsions des deux forces énergétiques se rencontrent, brûlent d'un même feu. Ce feu du sang est le feu du vampire. C'est le feu du silence sans mot, celui du devenir sans forme.

```{=tex}
\vspace{1\baselineskip}
```
**Réfutation : Le tantrisme est une farce. Les yogis ne sont que des puces.**

```{=tex}
\vspace{1\baselineskip}
```
`\uline{Il n'y a pas d'image possible autre que le sang, dans le \emph{devenir-vampire}.}`{=tex}

## Colonne du Vide

Les images n'ont plus lieu d'être. Les figures ne sont que des illusions d'*être*. Elles sont des vecteurs de compréhension lors de la traversée des états intermédiaires, vouées ainsi à disparaître. La colonne est établie verticalement dans un espace de vacuité.

```{=tex}
\vspace{1\baselineskip}
```
"... Noble ami·e, les images n'ont plus de visage. Tu peux maintenant envisager l'*inenvisageable*..."

```{=tex}
\vspace{1\baselineskip}
```
La colonne du vide est le *canal subtil* du souffle. Ses parois n'ont d'autres limites que la réfutation perpétuelle des limites de l'existant.

La colonne résiste à la *prédation* des phénomènes.

La cage thoracique est le refuge de la colonne.

Le *Vide Apatride* est inaltérable.

### Quatrième commentaire sur le devenir-vampire

```{=tex}
\hspace{1em}
```
La *libération*, comme dans le *Bardo*[^1-traite-du-vide-apatride_10], ne peut advenir que par l'Écoute. Ce qui implique la capacité d'accueillir ce qui n'est pas la parole, ou ce qui est autre dans la parole, c'est-à-dire la langue apatride.

L'Écoute dans le *seon* est un engagement total de l'*être* dans la connaissance de l'*être*, dans la pratique de l'*être* comme expérience fondamentale, pratiquée sans la mise à distance de la pensée comme représentation des phénomènes de l'*étant*.

L'Écoute dans le *seon* est une attention profonde à ce qui advient. Elle n'est pas une attitude passive, mais au contraire une pratique de connaissance.

```{=tex}
\vspace{1\baselineskip}
```
Le *seon* est une pratique de l'*être*.

# Vide Apatride

## Devenir-vampire dans le Vide Apatride

"Noble ami·e, dès le commencement, les phénomènes sont vides..."

"... Deux hétérocères blancs s'accouplent sur une pierre bleue..."

"... La grue qui s'envole ressemble à un squelette. La colonne de nuages ressemble à une colonne vertébrale..."

```{=tex}
\asterisme
```
Qui fera l'expérience de l'apatride n'aura plus besoin de langue pour dire l'apatride.

```{=tex}
\asterisme
```
Le vampire apatride a une langue de sang. Sa langue ne parle pas avec la langue, mais avec le sang.

```{=tex}
\asterisme
```
Le vampire apatride ne répond pas aux phénomènes.

```{=tex}
\asterisme
```
Le vampire apatride ne répond pas aux pulsions.

```{=tex}
\asterisme
```
L'apatride est sans pulsion.

```{=tex}
\asterisme
```
Le vampire devient vampire dans le *Vide Apatride*.

```{=tex}
\asterisme
```
Le vampire est apatride ou il n'est pas vampire.

```{=tex}
\asterisme
```
Le vampire devient vampire quand il oublie qu'il est vampire.

```{=tex}
\asterisme
```
Si le vampire ne parle pas, alors il parle en vampire.

```{=tex}
\asterisme
```
Le silence est la condition du *devenir-vampire* dans le *Vide Apatride*.

```{=tex}
\asterisme
```
Le silence parle vampire comme le vampire parle en silence.

```{=tex}
\asterisme
```
Le silence est une formulation de l'apatride. Le silence contient toutes les formulations de l'apatride.

```{=tex}
\asterisme
```
Le silence conditionne l'acuité. Le *devenir-vampire*, en tant que devenir, laisse advenir le monde dans le silence. Par le silence, il devient monde.

```{=tex}
\asterisme
```
Le *Vide Apatride* est un devenir où n'advient rien d'autre que le devenir.

```{=tex}
\asterisme
```
Le devenir en lui-même ne peut contenir le monde, mais le monde est en lui-même devenir.

```{=tex}
\asterisme
```
Le monde est un devenir insatiable. En ce sens, il est vampire.

```{=tex}
\asterisme
```
L'apatride est limpide.

```{=tex}
\asterisme
```
L'apatride est un monde dégagé du monde.

```{=tex}
\asterisme
```
L'apatride n'est pas une langue. L'apatride est dans toutes les langues.

```{=tex}
\asterisme
```
La pensée analysante paralyse. Le *Vide Apatride* est un devenir perpétuellement *en voie* vers un autre devenir. Il est sans limites.

```{=tex}
\asterisme
```
Le *Vide Apatride* est l'attente inattendue.

```{=tex}
\asterisme
```
Dans le *Vide Apatride*, le vampire est sans cesse déporté, décentré.

```{=tex}
\asterisme
```
Dans ce mouvement perpétuel, il trouve son centre.

```{=tex}
\vspace{1\baselineskip}

{\begin{nscenter}\notosymbols ◗ ◖\end{nscenter}}

\vspace{1\baselineskip}
```
"Noble ami·e devenu·e vampire, ne cherche pas tes mots, la *vérité en soi* est sensitive. La plante aussi est vampire. La célosie est vampire..."

"... En rongeant le radis, en rongeant la pastèque, le rat, sans le savoir, crée un cercle..."

"... Le savoir de la *vérité en soi* est sensitif..."

"... Le savoir de la *vérité en soi* est hématique..."

"... Le rat est le vampire ; le fruit et la racine, le sang..."

"... Les *mudras* sont des *mudras* en dents de vampire..."

"Noble ami·e, il se pourrait bien que le vide donne naissance à des fleurs..."

```{=tex}
\begin{chassefixe}
nous avons versé le sang de l'oiseau dans une assiette et le sang était rouge et il était sombre aussi

il faisait nuit et il y avait les grands bananiers qui remuaient autour de nous

le sang dans l'assiette faisait une tache rouge et nous attendions en silence qu'un esprit se manifeste

il y avait le sang et il y avait notre souffle et le mouvement des feuilles qui se balançaient dans la nuit

les \emph{colugos} rampaient sur les arbres et parfois nous pouvions entendre le bruit d'un animal qui sautait de branche en branche

nous avons marché dans la nuit avec la coupe de sang dans les mains pour attirer un esprit

le serpentaire \emph{bacha} dormait dans un sophora et les passereaux s'envolaient sur les rochers noirs

les yeux des serpents brillaient et leurs écailles ressemblaient à du métal

j'ai pris tes mains et j'ai mis tes doigts dans l'assiette pour qu'ils deviennent rouges et j'ai mis ton doigt dans ma bouche

je me souviens de t'avoir dit que les oiseaux aussi pouvaient devenir vampires

nous nous sommes accroupis dans la nuit et un \emph{colugo} est passé juste au-dessus de nous volant d'un arbre à l'autre

nous avons entendu tout près les pas d'un \emph{tamandua} et derrière un fourré nous l'avons vu se déplacer sur la terre et son pelage était si blanc que nous nous sommes demandé s'il ne s'agissait pas d'un esprit venu pour le sang et alors tu m'as répondu qu'il s'agissait bien d'un esprit vampire venu pour le sang

je t'ai dit que je n'avais plus de mémoire et que la mémoire aussi pouvait mourir et que la mémoire aussi pouvait couler comme le sang que nous avons versé sur la terre

nous avons craché sur le sol et le sang se mélangeait à la terre et à notre salive et les oiseaux ont commencé à s'agiter dans les sophoras et les feuilles des bananiers se balançaient encore sous le ciel noir

nous nous sommes embrassés et le sang sur mes lèvres a marqué tes lèvres

tu traçais des lignes dans la terre avec ton doigt et tes cheveux noirs couvraient ton visage quand tu te penchais en avant pour regarder le sol

je ne voyais plus tes yeux mais tes lèvres tachées de sang apparaissaient à travers ta chevelure qui recouvrait ton visage comme de l'eau sombre

je pense que nous étions devenus des vampires sans le savoir

je pense que nous ne pensions plus à rien dans cette nuit vampire

le sang a séché sur mes lèvres et quand je regardais tes yeux je les trouvais noirs comme le ciel ou peut-être plus noirs encore que le ciel

tu m'as dit que le \emph{tamandua} que nous avions vu s'était transformé et qu'il était devenu une chauve-souris ou un \emph{colugo}

nous sommes devenus vampires parce que nous pouvions voir l'Invisible

nous sommes devenus vampires parce que nous étions en train de toucher l'Invisible

tu m'as dit que les esprits sont dans le sang comme la mémoire est dans le sang et comme la flore et la faune s'ouvrent et s'animent dans le sang

il y a une lumière dans le sang et cette lumière est la lumière du vampire

nous deux vampires encore nous nous sommes endormis et pour la première fois j'ai vu les étoiles dans mon sommeil

dans mon rêve je voyais distinctement les étoiles et elles étaient toutes \emph{à leur place}

le ciel était le sang et les étoiles étaient les globules rouges et blancs dans leur plasma céleste

nous dormions tous les deux ensemble et notre sang animait nos rêves de vampires
\end{chassefixe}
```
## Seon Apatride

User de plusieurs langues, pour n'en avoir aucune.

### Expériences du seon dans la pratique du seon I

```{=tex}
\hspace{1em}
```
Dans les premières minutes, des voix résonnent, sans lieu comme le *feu de la langue*.

Le *feu de la langue* est un résidu de voix entendues, des résidus de paroles qui ont laissé leur empreinte sur le mental. Ces voix n'ont aucune logique langagière, elles sont de pures marques sur le mental qui les *régurgite*. Elles tournent comme des résidus dans le psychisme.

Ces *feux de la langue* sont la marque d'une saturation des signaux appréhendés par les sens. Dans le *seon*, le vampire prend conscience des résidus qui troublent le devenir.

Le vampire doit se défaire du *feu de la langue* en pratiquant le *seon*. Le mental laisse ainsi se tarir le feu, il s'en désolidarise, en plaçant le mental à distance.

Le *centre* de l'attention est déplacé. Le *centre* est l'acuité seule.

Le vampire ne refuse rien, mais ne retient rien. Il n'y a plus d'analyse des phénomènes ou des représentations.

Dans le devenir, le vampire se place lui-même en tant que devenir.

Ce ne sont pas les états intermédiaires qui le retiennent dans un état stagnant, mais les changements d'état, par leurs successions même, qui sont *reconnus* par lui comme seuls états d'une réalité impermanente.

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, un espace s'ouvre dans ta cage thoracique. Tu deviens un objet tellurique sans nom. Tu pourrais devenir une flamme de sang. Tu deviens une flamme vampire..."

"... dans le *Vide Apatride*, tu deviens..."

"... tu deviendras objet tellurique... aussi calme que le *seonbawi*[^1-traite-du-vide-apatride_11]..."

"... Dans l'érosion il y a, non pas la disparition, mais le devenir des formes. Le vampire voit le devenir fluide de la pierre, l'organisme et ses transformations, comme le sang circule dans l'immobilité du corps..."

"... ton corps est une fleur de sang. Ton corps est aussi fin qu'une feuille de sang. Ton corps est aussi léger qu'une goutte de sang..."

"Noble ami·e, tu es un être de sang. Alors, deviens vampire dans le sang..."

"... en tant qu'être de sang, tu dois aimer le sang..."

```{=tex}
\clearpage
```
### Expériences du seon dans la pratique du seon II

```{=tex}
\hspace{1em}
```
Quand les *langues de feu* se seront éteintes, la seule qui se manifestera, par delà toutes les autres, sera la langue apatride.

L'immobilité du corps vampire apporte une lumière aux flux invisibles, au sang circulant dans les organes, ainsi qu'au souffle traversant le corps. Le vampire respire par le corps, non seulement dans les poumons. Sang et souffle sont unifiés. Le cœur vampire est pacifié dans le mouvement du souffle lié au mouvement du sang. Ce mouvement unique est le mouvement du *devenir-vampire* dans le *Vide Apatride*.

Ce qui monte, ce qui circule en silence dans la poitrine, relie les flux de pensée au flux du sang, au souffle.

Le souffle, le sang, la pensée sont une seule langue dans le *devenir*. C'est la langue de l'apatride.

### Vampire du Vide dans le devenir apatride

```{=tex}
\hspace{1em}
```
"... Noble ami·e, détaché·e des illusions du lieu, tu découvres l'existence du *sans-lieu*. Détaché·e des illusions des phénomènes de la langue et de la pensée, tu découvres l'existence de l'apatride..."

"... L'apatride parle en tant que langue du *devenir*..."

"... La langue apatride peut tout dire. Elle exprime le *devenir* par le vide. Elle exprime le vide dans le *devenir*. Le vide est le cœur du *devenir*. Le *devenir* est le cœur du Vide..."

"... Le vide est le *devenir* sans paroles. Le vide est le *devenir* de toute parole..."

"Noble ami·e, vampire de chair, dans les yeux du *colugo*, dans les yeux du *tamandua*, tu verras..."

"... tes mémoires et la mutation de tes mémoires, et la vie de ceux qui t'ont donné la vie..."

```{=tex}
\begin{chassefixe}
dans les yeux du \emph{tamandua} je me voyais être le \emph{tamandua}

j'ai arrêté de penser parce que j'ai compris que j'étais cet animal et que cet animal lui aussi \mbox{se reconnaissait} en moi

dans les feuilles des arbres il y avait le murmure d'une langue et cette langue était une langue sans langue mais une langue commune à toutes les langues

je parlais la langue du \emph{tamandua} parce que j'étais le \emph{tamandua}

ma mémoire était aussi dans celle du \emph{tamandua} et dans ma mémoire je percevais celle du \emph{tamandua}

\sout{nous nous sommes reconnus et je me suis reconnu en nous}
\end{chassefixe}
```
### Expériences du seon dans la pratique du seon III

```{=tex}
\hspace{1em}
```
Laisser s'évanouir d'elles-mêmes les projections du mental.

Les phénomènes adipeux sont écartés par le *Vide Apatride*.

Regard apatride. La tension oculaire est inexistante. Les yeux à demi ouverts, le regard s'ouvre, mais ne fixe rien.

Le regard apatride se vide de toutes les images et des projections adipeuses du visible.

Les phénomènes en mouvement paralysent.

L'immobilité du *seon* libère les flux internes et invisibles qui sont les racines du mouvement.

```{=tex}
\vspace{1\baselineskip}
```
"Noble ami·e, être sans épaisseur, en disparaissant au regard des autres, tu t'apparais à toi-même..."

"... le souffle et le sang dans l'apatride sont un monde de silence qui s'ouvre sur des mondes de silence..."

```{=tex}
\vspace{1\baselineskip}
```
Les mutations du vivant s'enracinent dans l'invisible.

Les racines du visible mutent dans le vide.

```{=tex}
\vspace{1\baselineskip}
```
Les mutations du vivant sont toutes produites dans le *Vide Apatride*.

```{=tex}
\begin{chassefixe}
tu t'es réveillée sous le grand bananier et je t'ai demandé si tu avais ressenti toi aussi le vide autour de nous et en nous

tu m'as répondu que le vide était dans le monde et son mouvement et dans ce monde qui n'en finissait jamais de se transformer

tu m'as dit aussi que le vide dans le monde qui nous entourait était le même que celui qui était en nous et que ce qui était en dehors était aussi en dedans et que ce qui était lointain était à la fois ce qui était proche

tu m'as dit que le \emph{tamandua} qui était apparu dans ton rêve était aussi celui qui était passé tout près de nous et de nos deux corps endormis dans la nuit

et que ces deux mondes qui se visitaient étaient aussi traversés du même vide parce qu'ils étaient tous les deux des mondes en mutation dans le \emph{Vide Apatride}

alors après avoir parlé nous n'avons plus parlé et tu m'as montré ton poing fermé et après quelques secondes tu l'as ouvert et j'ai vu qu'il n'y avait rien dans ta main

tu as souri avec tes dents de vampire

et avec tes deux mains de vampire tu as formé la \emph{mudra cosmique}

et nous sommes devenus encore ce que nous étions avant de devenir...

\vspace{1\baselineskip}

... des vampires dans le \emph{Vide Apatride}
\end{chassefixe}
```

[^1-traite-du-vide-apatride_1]: Versant coréen du *chán* chinois, ou *zen* japonais\.

[^1-traite-du-vide-apatride_2]: *Māra*. Abrüpt, Zürich, 2021\.

[^1-traite-du-vide-apatride_3]: Ici, dans le sens de "sortilèges"\.

[^1-traite-du-vide-apatride_4]: *Le Secret de la Fleur d'Or*. C. G. Jung, Rascher, Zürich, 1938\.

[^1-traite-du-vide-apatride_5]: *Agonie des Eros*. Byung-Chul Han, Matthes & Seitz, Berlin, 2012\.

[^1-traite-du-vide-apatride_6]: *Octologo*. Giacinto Scelsi, le parole gelate, Rome, 1987\.

[^1-traite-du-vide-apatride_7]: Morsure des sortilèges\.

[^1-traite-du-vide-apatride_8]: *Traité du Milieu, Analyse des Agrégats*. Nagarjuna\.

[^1-traite-du-vide-apatride_9]: Chamane coréenne, presque toujours une femme\.

[^1-traite-du-vide-apatride_10]: Le *Bardo*, lui-même, Livre de la Libération par l'Écoute\.

[^1-traite-du-vide-apatride_11]: Littéralement, le rocher *seon*, qui se trouve sur les flancs du mont Inwangsan à Séoul, à proximité du sanctuaire chamane Guksadang\.
