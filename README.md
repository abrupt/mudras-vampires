# ~/ABRÜPT/ÉTIENNE MICHELET/MUDRAS VAMPIRES/*

La [page de ce livre](https://abrupt.cc/etienne-michelet/mudras-vampires/) sur le réseau.

## Sur le livre

Je suis devenu vampire par la pratique du seon. Mais je n’aurais jamais découvert le seon sans mener une existence vampire en Corée.

Ce livre n’est pas un livre de développement personnel, ou il l’est, mais à l’usage des vampires.

## Sur l'auteur

Né en 1984. Vit en Corée du Sud. Y écrit. Publie et dirige le projet Preta ainsi que la <a href="https://www.pretamemori.com/" target="_blank">revue Memori</a>.

Travaille actuellement à une théorie vampire sur l'expérience méditative du Seon.

Son site : <a href="https://www.dongmuri.net/" target="_blank">DONGMURI</a>.

## Sur la licence

### Texte

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](LICENSE-TXT)).

### HTML

La version HTML de cet antilivre est placée sous [licence MIT](LICENCE-MIT).

### Sculpture

L'image de couverture et la version HTML se basent sur une sculpture originale produite à partir d'objets 3D libres, créés généreusement par [取名字好难啊](https://sketchfab.com/12241218) et [Paruhus Denchai](https://www.turbosquid.com/Search/Artists/Paruhus--Denchai). Nous les remercions et nous plaçons en conséquence cette nouvelle sculpture, ainsi que l'image de couverture, sous licence Creative Commons Attribution 4.0 International ([CC BY 4.0](LICENSE-COVER)).


## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
